package com.beevishapps.hitright;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class HighscoresActivity extends AppCompatActivity {


    TextView highScoreLabel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_highscores);

        Button letsBreakIt=(Button)findViewById(R.id.letsbreakit);
        letsBreakIt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), SinglePlayer.class);
                startActivity(i);
            }
        });


  highScoreLabel=(TextView)findViewById(R.id.highscoreLabel);

        SharedPreferences prefs = getSharedPreferences("HIGH_SCORE", MODE_PRIVATE);

        int SavedHighScore = prefs.getInt("HIGH_SCORE", 0);
//        if (SavedHighScore != 0) {
           // String name = prefs.getString("name", "No name defined");//"No name defined" is the default value.
//            int checkHighScore = prefs.getInt("checkHighScore", 0); //0 is the default value.

            highScoreLabel.setText("Single Player :"+SavedHighScore);

//        }





    }
}
