package com.beevishapps.hitright;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

public class MultiSection extends AppCompatActivity {


    FloatingActionButton button1, button2, button3, button4, button5,
            button6, button7, button8, button9, button10,
            button11, button12, button13, button14, button15,
            button16, button17, button18, button19, button20,
            button21, button22, button23, button24, button25,
            button26, button27, button28, button29, button30,
            button31, button32, button33, button34, button35,
            button36, button37, button38, button39, button40;

    TextView msp1points, msp2points;

    int visibilityToReaapear = 7000;
    int timeToReaapear = 3000;
    int buttonChangeColortime = 2000;
    LinearLayout gameArea1, gameArea2;
    int player1points = 0, player2points = 0;

    //popup code
    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_multi_section);

//        Intent svc=new Intent(getApplicationContext(), BackgroundSoundService.class);
//        stopService(svc);//................................................................................................................stopping bubbling sound


        Animation slideDown = AnimationUtils.loadAnimation(this, R.anim.slide_down);//..........................................loading animation to layout
        Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);//..........................................loading animation to layout


        gameArea1 = (LinearLayout) findViewById(R.id.gameAreaPlayer1);
        gameArea2 = (LinearLayout) findViewById(R.id.gameAreaPlayer2);
        gameArea1.startAnimation(slideUp);//.............................................................................layout animation moving down to up for player1
        gameArea2.startAnimation(slideDown);//...........................................................................layout animation moving up to down for player2

        disable(gameArea1);//................................................................................................................disabling view for running 3 2 1
        disable(gameArea2);




        //**************************************************************************************************************321 Start************************************************************************************************************
        final Handler handler321 = new Handler();
        final TextView textView123 = (TextView) findViewById(R.id.textView123);

        final java.util.concurrent.atomic.AtomicInteger n = new AtomicInteger(3);

//        MediaPlayer buttonBounce = MediaPlayer.create(this, R.raw.gomusic);
//        buttonBounce.start();

        final Runnable counter = new Runnable() {
            @Override
            public void run() {
                textView123.setText(Integer.toString(n.get()));
                if (n.getAndDecrement() >= 1)
                    handler321.postDelayed(this, 380);
                else {
                    textView123.setText("GO");

                    final Handler GoVisiblity = new Handler();//....................................................................pausing "GO" for one second and then making it invisible
                    GoVisiblity.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            textView123.setVisibility(View.GONE);
                        }
                    }, 1000);


                    // start the game
                    Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    vib.vibrate(500);//................................................................................................................vibrating Device
                    enable(gameArea1);//......................................................................enabling view after running 3 2 1
                    enable(gameArea2);
//                    enable(MultiParent);//................................................................................................................enabling view after running 3 2 1
                    button13.performClick();//................................................................................intial click to start the process
                    button28.performClick();
                }
            }
        };
        handler321.postDelayed(counter, 500);//............................................................................time taken to start displaying 321
//************************************************************************************************************END__321 Start*********************************************************************************


        myDialog = new Dialog(this);//.......................................................................................................for pop up layout


        //Now the buttons
        button1 = (FloatingActionButton) findViewById(R.id.button1);
        button2 = (FloatingActionButton) findViewById(R.id.button2);
        button3 = (FloatingActionButton) findViewById(R.id.button3);
        button4 = (FloatingActionButton) findViewById(R.id.button4);
        button5 = (FloatingActionButton) findViewById(R.id.button5);
        button6 = (FloatingActionButton) findViewById(R.id.button6);
        button7 = (FloatingActionButton) findViewById(R.id.button7);
        button8 = (FloatingActionButton) findViewById(R.id.button8);
        button9 = (FloatingActionButton) findViewById(R.id.button9);
        button10 = (FloatingActionButton) findViewById(R.id.button10);
        button11 = (FloatingActionButton) findViewById(R.id.button11);
        button12 = (FloatingActionButton) findViewById(R.id.button12);
        button13 = (FloatingActionButton) findViewById(R.id.button13);
        button14 = (FloatingActionButton) findViewById(R.id.button14);
        button15 = (FloatingActionButton) findViewById(R.id.button15);
        button16 = (FloatingActionButton) findViewById(R.id.button16);
        button17 = (FloatingActionButton) findViewById(R.id.button17);
        button18 = (FloatingActionButton) findViewById(R.id.button18);
        button19 = (FloatingActionButton) findViewById(R.id.button19);
        button20 = (FloatingActionButton) findViewById(R.id.button20);
        button21 = (FloatingActionButton) findViewById(R.id.button21);
        button22 = (FloatingActionButton) findViewById(R.id.button22);
        button23 = (FloatingActionButton) findViewById(R.id.button23);
        button24 = (FloatingActionButton) findViewById(R.id.button24);
        button25 = (FloatingActionButton) findViewById(R.id.button25);
        button26 = (FloatingActionButton) findViewById(R.id.button26);
        button27 = (FloatingActionButton) findViewById(R.id.button27);
        button28 = (FloatingActionButton) findViewById(R.id.button28);
        button29 = (FloatingActionButton) findViewById(R.id.button29);
        button30 = (FloatingActionButton) findViewById(R.id.button30);
        button31 = (FloatingActionButton) findViewById(R.id.button31);
        button32 = (FloatingActionButton) findViewById(R.id.button32);
        button33 = (FloatingActionButton) findViewById(R.id.button33);
        button34 = (FloatingActionButton) findViewById(R.id.button34);
        button35 = (FloatingActionButton) findViewById(R.id.button35);
        button36 = (FloatingActionButton) findViewById(R.id.button36);
        button37 = (FloatingActionButton) findViewById(R.id.button37);
        button38 = (FloatingActionButton) findViewById(R.id.button38);
        button39 = (FloatingActionButton) findViewById(R.id.button39);
        button40 = (FloatingActionButton) findViewById(R.id.button40);


        //Now set all the buttons to listen for clicks
        button1.setOnClickListener(player2Click);
        button2.setOnClickListener(player2Click);
        button3.setOnClickListener(player2Click);
        button4.setOnClickListener(player2Click);
        button5.setOnClickListener(player2Click);
        button6.setOnClickListener(player2Click);
        button7.setOnClickListener(player2Click);
        button8.setOnClickListener(player2Click);
        button9.setOnClickListener(player2Click);
        button10.setOnClickListener(player2Click);
        button11.setOnClickListener(player2Click);
        button12.setOnClickListener(player2Click);
        button13.setOnClickListener(player2Click);
        button14.setOnClickListener(player2Click);
        button15.setOnClickListener(player2Click);
        button16.setOnClickListener(player2Click);
        button17.setOnClickListener(player2Click);
        button18.setOnClickListener(player2Click);
        button19.setOnClickListener(player2Click);
        button20.setOnClickListener(player2Click);


        button21.setOnClickListener(player1Click);
        button22.setOnClickListener(player1Click);
        button23.setOnClickListener(player1Click);
        button24.setOnClickListener(player1Click);
        button25.setOnClickListener(player1Click);
        button26.setOnClickListener(player1Click);
        button27.setOnClickListener(player1Click);
        button28.setOnClickListener(player1Click);
        button29.setOnClickListener(player1Click);
        button30.setOnClickListener(player1Click);
        button31.setOnClickListener(player1Click);
        button32.setOnClickListener(player1Click);
        button33.setOnClickListener(player1Click);
        button34.setOnClickListener(player1Click);
        button35.setOnClickListener(player1Click);
        button36.setOnClickListener(player1Click);
        button37.setOnClickListener(player1Click);
        button38.setOnClickListener(player1Click);
        button39.setOnClickListener(player1Click);
        button40.setOnClickListener(player1Click);

    }


    View.OnClickListener player1Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            //getting button by its id according to its click

            final FloatingActionButton clickedButton;
            clickedButton = (FloatingActionButton) findViewById(v.getId());


//*******************************************************************Visiblity of button********************************************************************************************


            clickedButton.setVisibility(View.INVISIBLE);
            final Handler handlerclickedbuttonVisibility = new Handler();
            handlerclickedbuttonVisibility.postDelayed(new Runnable() {
                @Override
                public void run() {
                    clickedButton.setVisibility(View.VISIBLE);
                }
            }, visibilityToReaapear);

            visibilityToReaapear = visibilityToReaapear - 20;


//*******************************************************************END__Visiblity of button********************************************************************************************


            //*********************************************************************calling setting all buttons color  function after giving some time to load and repeating the process*************************************************


            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    // Your logic here...

                    // When you need to modify a UI element, do so on the UI thread.
                    // 'getActivity()' is required as this is being ran from a Fragment.
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // This code will always run on the UI thread, therefore is safe to modify UI elements.


                            allButtonRandomChange4();
                            allButtonRandomChange6();
                            allButtonRandomChange2();
                            allButtonRandomChange5();
                            allButtonRandomChange3();
                            allButtonRandomChange1();


                        }
                    });
                }
            }, 0, buttonChangeColortime + 5000); // the code will run twice(2) after specified time(buttonChangeColortime) for infinte period of time.


//*********************************************************************END__calling setting all buttons color function after giving some time to load and repeating the process*************************************************


            //*************************************************Animation************************************************
// http://evgenii.com/blog/spring-button-animation-on-android/

            final Animation myAnim = AnimationUtils.loadAnimation(MultiSection.this, R.anim.bounce);

            // Use bounce interpolator with amplitude 0.2 and frequency 20
            MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
            myAnim.setInterpolator(interpolator);

            clickedButton.startAnimation(myAnim);
//*************************************************End___Animation************************************************

            msp1points = (TextView) findViewById(R.id.msp1points);
            String player1Color = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());
            String white = "-1";
            String black = "-16777216";


            if (player1Color.equals(white)) {
                player1points = player1points + 2;
            }

            if (player1Color.equals(black)) {
                Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                vib.vibrate(500);//.....................................................................................................................................vibrating Device
                ShowPopup("PLAYER 2");
            } else {
                player1points--;
            }
            msp1points.setText(String.valueOf(player1points));


            //calling setting button color function after giving some time to load

            final Handler handlerclickedbutton = new Handler();
            handlerclickedbutton.postDelayed(new Runnable() {
                @Override
                public void run() {
                    setNewColorToButton(clickedButton);
                }
            }, timeToReaapear);

            timeToReaapear = timeToReaapear - 20;


        }
    };


    View.OnClickListener player2Click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //getting button by its id according to its click

            final FloatingActionButton clickedButton;
            clickedButton = (FloatingActionButton) findViewById(v.getId());


//*******************************************************************Visiblity of button********************************************************************************************


            clickedButton.setVisibility(View.INVISIBLE);
            final Handler handlerclickedbuttonVisibility = new Handler();
            handlerclickedbuttonVisibility.postDelayed(new Runnable() {
                @Override
                public void run() {
                    clickedButton.setVisibility(View.VISIBLE);
                }
            }, visibilityToReaapear);

            visibilityToReaapear = visibilityToReaapear - 20;


//*******************************************************************END__Visiblity of button********************************************************************************************


            //*********************************************************************calling setting all buttons color  function after giving some time to load and repeating the process*************************************************


            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    // Your logic here...

                    // When you need to modify a UI element, do so on the UI thread.
                    // 'getActivity()' is required as this is being ran from a Fragment.
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // This code will always run on the UI thread, therefore is safe to modify UI elements.


                            allButtonRandomChange4();
                            allButtonRandomChange6();
                            allButtonRandomChange2();
                            allButtonRandomChange5();
                            allButtonRandomChange3();
                            allButtonRandomChange1();


                        }
                    });
                }
            }, 0, buttonChangeColortime + 5000); // the code will run twice(2) after specified time(buttonChangeColortime) for infinte period of time.


//*********************************************************************END__calling setting all buttons color function after giving some time to load and repeating the process*************************************************


            //*************************************************Animation************************************************
// http://evgenii.com/blog/spring-button-animation-on-android/

            final Animation myAnim = AnimationUtils.loadAnimation(MultiSection.this, R.anim.bounce);

            // Use bounce interpolator with amplitude 0.2 and frequency 20
            MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
            myAnim.setInterpolator(interpolator);

            clickedButton.startAnimation(myAnim);
//*************************************************End___Animation************************************************

            msp2points = (TextView) findViewById(R.id.msp2points);
            String player2Color = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());
            String black = "-16777216";
            String white = "-1";


            if (player2Color.equals(black)) {
                player2points = player2points + 2;
            }
            if (player2Color.equals(white)) {
                Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                vib.vibrate(500);//.....................................................................................................................................vibrating Device
                ShowPopup("PLAYER 1");
            } else {
                player2points--;
            }

            msp2points.setText(String.valueOf(player2points));


            //calling setting button color function after giving some time to load

            final Handler handlerclickedbutton = new Handler();
            handlerclickedbutton.postDelayed(new Runnable() {
                @Override
                public void run() {
                    setNewColorToButton(clickedButton);
                }
            }, timeToReaapear);

            timeToReaapear = timeToReaapear - 20;


        }
    };


    //*************************************************************************************************Popup after Losing the Game**********************************************************
    public void ShowPopup(String winner) {


        TextView txtclose, txtLoser;
        Button btnRestart, btnQuit;

        myDialog.setContentView(R.layout.custompopuplose);

        myDialog.setCanceledOnTouchOutside(false);//............................................................................................................diabling touching outside the myDialog box.

        myDialog.setCancelable(false);//.........................disabling the cancel button from closing the dialog

//        txtclose = (TextView) myDialog.findViewById(R.id.txtclose);
        txtLoser = (TextView) myDialog.findViewById(R.id.loser);
        btnRestart = (Button) myDialog.findViewById(R.id.btnrestart);
        btnQuit = (Button) myDialog.findViewById(R.id.btnquit);

        txtLoser.setText(winner + " WINS");//............................................................................................................setting winner

        btnQuit.setOnClickListener(new View.OnClickListener() {//.............................................Quit Button
            @Override
            public void onClick(View view) {

//...................................................passing EXIT in intent and clearly activity stack so that this activity can recognise that it has been called before and should not reload again
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("EXIT", true);
                startActivity(i);


            }
        });

        btnRestart.setOnClickListener(new View.OnClickListener() {//.............................................Restart Button
            @Override
            public void onClick(View view) {


                Intent intent = getIntent();
                finish();
                startActivity(intent);

            }
        });

//        txtclose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                myDialog.dismiss();
//            }
//        });

        myDialog.getWindow().getAttributes().windowAnimations = R.style.animationName;//...................showing animation while popup appears
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();

    }
    //*************************************************************************************************END__Popup after Losing the game**********************************************************


    //*******************************************************************Randomising and Setting new color to the the Single button function*************************************************
    private void setNewColorToButton(FloatingActionButton clickedButton) {


        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //....................................................number betwween 1(inclusive) and 5(exclusive).....

        //..........if the number is zero.....
        //myRandomColor++;


        switch (myRandomColor) {
            case 1:

                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.msplayer1));

                break;
            case 2:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.msplayer2));
                break;
            case 3:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.msplayer1));
                break;
            case 4:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.msplayer2));
                break;

            default:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.msplayer1));//.......should never go in this


        }


    }

//*******************************************************************END__Randomising and Setting new color to the the Single button function*************************************************


    //******************************************************************Randomly changing colors of buttons******************************************************************
    private void allButtonRandomChange1() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................numver betwween 1(inclusive) and 5(exclusive)

        switch (myRandomColor) {
            case 1:
                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button2.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button3.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


                break;
            case 2:
                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button10.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button3.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 3:
                button3.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button9.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button15.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 4:
                button2.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button8.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button14.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;

            default:
                button1.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button7.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button25.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange2() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //..........numver betwween 1(inclusive) and 5(exclusive).....

        switch (myRandomColor) {
            case 1:
                button1.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button7.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button25.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


                break;
            case 2:
                button6.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button12.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button24.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button30.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;
            case 3:
                button11.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button23.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button29.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button35.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 4:
                button16.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button22.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button28.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button34.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button40.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button35.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;

            default:
                button16.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));//...................................no use
                button22.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button28.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button34.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button35.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange3() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................number betwween 1(inclusive) and 5(exclusive)

        switch (myRandomColor) {
            case 1:
                button21.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button27.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button33.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button39.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


                break;
            case 2:
                button26.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button32.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button38.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 3:
                button31.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button37.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 4:
                button1.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button36.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;

            default:
                button1.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange4() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................number between 1(inclusive) and 5(exclusive).....

        switch (myRandomColor) {
            case 1:
                button6.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button2.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


                break;
            case 2:
                button11.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button7.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button3.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 3:
                button16.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button12.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button8.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 4:
                button21.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button9.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;

            default:
                button21.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));//....................................no use
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button9.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange5() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................numver betwween 1(inclusive) and 5(exclusive).....

        switch (myRandomColor) {
            case 1:
                button26.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button22.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button14.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button10.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));


                break;
            case 2:
                button31.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button27.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button23.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button15.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;
            case 3:
                button36.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button32.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button28.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button24.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 4:
                button37.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button33.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button29.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button25.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;

            default:
                button33.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));//..................................no use
                button29.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button25.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange6() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................numver betwween 1(inclusive) and 5(exclusive).....

        switch (myRandomColor) {
            case 1:
                button38.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button34.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button30.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


                break;
            case 2:
                button39.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button35.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 3:
                button40.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button21.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button9.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 4:
                button40.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button26.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button22.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button14.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button10.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;

            default:
                button16.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }


    //*************************************************************************************************************************Enabling and Disabling View************************************************************************************
    private static void disable(ViewGroup layout) {
        layout.setEnabled(false);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                disable((ViewGroup) child);
            } else {
                child.setEnabled(false);
            }
        }
    }

    private static void enable(ViewGroup layout) {
        layout.setEnabled(true);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                enable((ViewGroup) child);
            } else {
                child.setEnabled(true);
            }
        }
    }
//*************************************************************************************************************************END__Enabling and Disabling View************************************************************************************


//******************************************************************END__Randomly changing colors of buttons******************************************************************

//    @Override
//    public void onBackPressed() {
//
//        Intent i = new Intent(getApplicationContext(), MainActivity.class);
//        startActivity(i);
//    }
}
