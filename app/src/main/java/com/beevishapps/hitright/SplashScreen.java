package com.beevishapps.hitright;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SplashScreen extends AppCompatActivity {


    MediaPlayer walkSoundstart,walkSoundend;
RelativeLayout splashParent;
    boolean isViewClicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screen);


//        String text = "<font color=#ffffff>V</font><font color=#cc0029>Nayak</font><br/><font color=#ffcc00>Games</font>";

        String text = "<font color=#ffffff>V</font><font color=#ffffff>Nayak</font><br/><font color=#FFEB3B>G</font><font color=#2196F3>a</font><font color=#11c612>m</font><font color=#4EFFF3>e</font><font color=#ef0276>s</font>";


        final TextView tv=(TextView)findViewById(R.id.trademark);
splashParent=(RelativeLayout)findViewById(R.id.splashParent);
        tv.setText(Html.fromHtml(text));//...............................................giving different text color to same text
//        final ImageView im=(ImageView)findViewById(R.id.splashback);
        final Animation an1= AnimationUtils.loadAnimation(getBaseContext(),R.anim.shakesplash);
        final Animation an2= AnimationUtils.loadAnimation(getBaseContext(),R.anim.right_swipe_splash);
//        final Animation an3= AnimationUtils.loadAnimation(getBaseContext(),R.anim.rotate);
        final Animation an4= AnimationUtils.loadAnimation(getBaseContext(),R.anim.right_out);

        final AnimationSet start = new AnimationSet(false);//.............................................adding two animation togerther
        start.addAnimation(an1);
        start.addAnimation(an2);

        final AnimationSet end = new AnimationSet(false);
        end.addAnimation(an1);
        end.addAnimation(an4);

//        im.startAnimation(an3);

        tv.startAnimation(start);
//        walkSoundstart = MediaPlayer.create(this, R.raw.walkingedit4);
        walkSoundend = MediaPlayer.create(this, R.raw.walkingedit7);
//        walkSoundstart.start();
        an2.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

//                tv.startAnimation(an1);
//                walkSoundstart.stop();
                walkSoundend.start();
                tv.startAnimation(end);


//                finish();
//                Intent i=new Intent(getApplicationContext(),MainActivity.class);
//                startActivity(i);
//                finish();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        //Do something after 100ms
                        walkSoundend.stop();
                        finish();

                        if(isViewClicked)
                        {

                        }
                        else
                        {
                            Intent i=new Intent(getApplicationContext(),MainActivity.class);
                            startActivity(i);
                        }

                    }
                }, 900);


            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

splashParent.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        isViewClicked=true;
        walkSoundend.stop();


        finish();


        Intent i = new Intent(getBaseContext(), MainActivity.class);
        startActivity(i);
    }
});

    }
}
