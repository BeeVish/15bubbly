package com.beevishapps.hitright;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class HwToPlay extends AppCompatActivity {


    Button playSingle,playMulti,playMultiSec;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hw_to_play);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);



        String textSingle = "<font color=#000000>1.Click ur own selected </font><font color=#D32F2F>B</font><font color=#2196F3>u</font><font color=#11c612>b</font><font color=#4EFFF3>b</font><font color=#ef0276>l</font>y.<br/><font>2.Keep a track on LifeWave.</font><br/><font>3.If you waste time your LifeWave will go down.</font>";

        String textMulti = "<font>1.Click Ur decided </font><font color=#D32F2F>B</font><font color=#2196F3>u</font><font color=#11c612>b</font><font color=#4EFFF3>b</font><font color=#ef0276>l</font>y anyWhere in Screen.<br/>2.First Player to score 100, Wins.</font>";

        String textMultiSec = "<font>1.Click Ur decided <font color=#D32F2F>B</font><font color=#2196F3>u</font><font color=#11c612>b</font><font color=#4EFFF3>b</font><font color=#ef0276>l</font>y in ur section itself.<br/>2.if u click opponents color u lose.</font>";


        final TextView tvSingle=(TextView)findViewById(R.id.hwtoPlay);

        final TextView tvMulti=(TextView)findViewById(R.id.hwtoPlayMulti);

        final TextView tvMultiSec=(TextView)findViewById(R.id.hwtoPlayMultiSection);

        tvSingle.setText(Html.fromHtml(textSingle));//...............................................giving different text color to same text

        tvMulti.setText(Html.fromHtml(textMulti));

        tvMultiSec.setText(Html.fromHtml(textMultiSec));



        playSingle=(Button)findViewById(R.id.buttonSingle);
        playMulti=(Button)findViewById(R.id.buttonMulti);
        playMultiSec=(Button)findViewById(R.id.buttonMultiSec);

        playSingle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(), SinglePlayer.class);
                startActivity(i);

            }
        });

        playMulti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(), GameActivity.class);
                startActivity(i);

            }
        });
        playMultiSec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(), MultiSection.class);
                startActivity(i);

            }
        });


//        Intent svc=new Intent(getApplicationContext(), BackgroundSoundService.class);
//        stopService(svc);//................................................................................................................stopping bubbling sound

    }
}
