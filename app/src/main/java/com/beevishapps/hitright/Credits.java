package com.beevishapps.hitright;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class Credits extends AppCompatActivity {

    TextView Concept;//Vinayakmac
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credits);


        Concept=(TextView)findViewById(R.id.Concept);
        Concept.setText("Omkar Deorukar & Vinayak Nair");
//        Intent svc=new Intent(getApplicationContext(), BackgroundSoundService.class);
//        stopService(svc);//................................................................................................................stopping bubbling sound

        ImageView titleimage = (ImageView) findViewById(R.id.titleImageView);
        Picasso.with(this).load(R.drawable.btitleimage).resize(2900,1000)
                .into(titleimage);


        final Animation forimage = AnimationUtils.loadAnimation(getBaseContext(), R.anim.slideupcredits);
        titleimage.startAnimation(forimage);

    }
}
