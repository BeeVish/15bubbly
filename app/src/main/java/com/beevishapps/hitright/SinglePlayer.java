package com.beevishapps.hitright;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import me.itangqi.waveloadingview.WaveLoadingView;


public class SinglePlayer extends AppCompatActivity implements View.OnClickListener {


    FloatingActionButton button1, button2, button3, button4, button5,
            button6, button7, button8, button9, button10,
            button11, button12, button13, button14, button15,
            button16, button17, button18, button19, button20,
            button21, button22, button23, button24, button25,
            button26, button27, button28, button29, button30,
            button31, button32, button33, button34, button35,
            button36, button37, button38, button39, button40,
            button41, button42, button43, button44, button45;

    //TextView Score,SinglePlayerPoints;


    RelativeLayout SingleParent;

    int HighScore;
    WaveLoadingView Life;

    TextView singlePlayerPoints, score;
    final Timer timer = new Timer();;
    int timeToReaapear = 3000;
    //popup code
    Dialog myDialog, myDialogGameover;

    //player selection of color
    int playerColorSelectedInt;
    String playerColorSelected = null;
    int playerColor;
    int delay = 3000;
    int visibilityToReaapear = 7000;
    int buttonChangeColortime = 5000;
    int playerPoints = 0;
    int lifeLevel = 100;
    String hexColor;

    int waveDie = Color.parseColor("#D32F2F");//#FF5252
    int waveFull = Color.parseColor("#1976D2");//#00e676  #4CAF50
    int wavecolor = Color.parseColor("#4EFFF3");//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        myDialog = new Dialog(this);//.............................................................................for color selelection and game over pop up layout

        //myDialogGameover = new Dialog(this);//.......................................................................................................for pop up layout

        ShowPopup(this);


        setContentView(R.layout.activity_single_player);

//        Intent svc=new Intent(getApplicationContext(), BackgroundSoundService.class);

//        stopService(svc);//................................................................................................................stopping bubbling sound


        SingleParent = (RelativeLayout) findViewById(R.id.SingleParent);

        SingleParent.setVisibility(View.INVISIBLE);//................................................................making relative layout invisible since myDailog is open
        disable(SingleParent);//................................................................................................................disabling view for running 3 2 1

        //Reference all the elements of our UI with an appropriate object

        score = (TextView) findViewById(R.id.score);
        singlePlayerPoints = (TextView) findViewById(R.id.singlePlayerPoints);


        //WaveLoadingView

        Life = (WaveLoadingView) findViewById(R.id.waveLoadingView);


        Life.setProgressValue(lifeLevel);
        Life.setWaveColor(waveFull);//.........setting color initially

        //Now the buttons
        button1 = (FloatingActionButton) findViewById(R.id.button1);
        button2 = (FloatingActionButton) findViewById(R.id.button2);
        button3 = (FloatingActionButton) findViewById(R.id.button3);
        button4 = (FloatingActionButton) findViewById(R.id.button4);
        button5 = (FloatingActionButton) findViewById(R.id.button5);
        button6 = (FloatingActionButton) findViewById(R.id.button6);
        button7 = (FloatingActionButton) findViewById(R.id.button7);
        button8 = (FloatingActionButton) findViewById(R.id.button8);
        button9 = (FloatingActionButton) findViewById(R.id.button9);
        button10 = (FloatingActionButton) findViewById(R.id.button10);
        button11 = (FloatingActionButton) findViewById(R.id.button11);
        button12 = (FloatingActionButton) findViewById(R.id.button12);
        button13 = (FloatingActionButton) findViewById(R.id.button13);
        button14 = (FloatingActionButton) findViewById(R.id.button14);
        button15 = (FloatingActionButton) findViewById(R.id.button15);
        button16 = (FloatingActionButton) findViewById(R.id.button16);
        button17 = (FloatingActionButton) findViewById(R.id.button17);
        button18 = (FloatingActionButton) findViewById(R.id.button18);
        button19 = (FloatingActionButton) findViewById(R.id.button19);
        button20 = (FloatingActionButton) findViewById(R.id.button20);
        button21 = (FloatingActionButton) findViewById(R.id.button21);
        button22 = (FloatingActionButton) findViewById(R.id.button22);
        button23 = (FloatingActionButton) findViewById(R.id.button23);
        button24 = (FloatingActionButton) findViewById(R.id.button24);
        button25 = (FloatingActionButton) findViewById(R.id.button25);
        button26 = (FloatingActionButton) findViewById(R.id.button26);
        button27 = (FloatingActionButton) findViewById(R.id.button27);
        button28 = (FloatingActionButton) findViewById(R.id.button28);
        button29 = (FloatingActionButton) findViewById(R.id.button29);
        button30 = (FloatingActionButton) findViewById(R.id.button30);
        button31 = (FloatingActionButton) findViewById(R.id.button31);
        button32 = (FloatingActionButton) findViewById(R.id.button32);
        button33 = (FloatingActionButton) findViewById(R.id.button33);
        button34 = (FloatingActionButton) findViewById(R.id.button34);
        button35 = (FloatingActionButton) findViewById(R.id.button35);
        button36 = (FloatingActionButton) findViewById(R.id.button36);
        button37 = (FloatingActionButton) findViewById(R.id.button37);
        button38 = (FloatingActionButton) findViewById(R.id.button38);
        button39 = (FloatingActionButton) findViewById(R.id.button39);
        button40 = (FloatingActionButton) findViewById(R.id.button40);
        button41 = (FloatingActionButton) findViewById(R.id.button41);
        button42 = (FloatingActionButton) findViewById(R.id.button42);
        button43 = (FloatingActionButton) findViewById(R.id.button43);
        button44 = (FloatingActionButton) findViewById(R.id.button44);
        button45 = (FloatingActionButton) findViewById(R.id.button45);






        //Now set all the buttons to listen for clicks
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);
        button10.setOnClickListener(this);
        button11.setOnClickListener(this);
        button12.setOnClickListener(this);
        button13.setOnClickListener(this);
        button14.setOnClickListener(this);
        button15.setOnClickListener(this);
        button16.setOnClickListener(this);
        button17.setOnClickListener(this);
        button18.setOnClickListener(this);
        button19.setOnClickListener(this);
        button20.setOnClickListener(this);
        button21.setOnClickListener(this);
        button22.setOnClickListener(this);
        button23.setOnClickListener(this);
        button24.setOnClickListener(this);
        button25.setOnClickListener(this);
        button26.setOnClickListener(this);
        button27.setOnClickListener(this);
        button28.setOnClickListener(this);
        button29.setOnClickListener(this);
        button30.setOnClickListener(this);
        button31.setOnClickListener(this);
        button32.setOnClickListener(this);
        button33.setOnClickListener(this);
        button34.setOnClickListener(this);
        button35.setOnClickListener(this);
        button36.setOnClickListener(this);
        button37.setOnClickListener(this);
        button38.setOnClickListener(this);
        button39.setOnClickListener(this);
        button40.setOnClickListener(this);
        button41.setOnClickListener(this);
        button42.setOnClickListener(this);
        button43.setOnClickListener(this);
        button44.setOnClickListener(this);
        button45.setOnClickListener(this);

        //Now TextViews
//    Score=(TextView)findViewById(R.id.score);
//    SinglePlayerPoints=(TextView)findViewById(R.id.singlePlayerPoints);
//
//        Score.setTextColor(playerColorSelectedInt);//...............................settting the color of score and points as color selected by the players
//
//        SinglePlayerPoints.setTextColor(playerColorSelectedInt);//....................................................settting the color of score and points as color selected by the players


    }


    @Override
    public void onClick(View v) {

        final FloatingActionButton clickedButton;
        clickedButton = (FloatingActionButton) findViewById(v.getId());


        //*******************************************************************Visiblity of button********************************************************************************************


        clickedButton.setVisibility(View.INVISIBLE);
        final Handler handlerclickedbuttonVisibility = new Handler();
        handlerclickedbuttonVisibility.postDelayed(new Runnable() {
            @Override
            public void run() {
                clickedButton.setVisibility(View.VISIBLE);
            }
        }, visibilityToReaapear);

        visibilityToReaapear = visibilityToReaapear - 20;


//*******************************************************************END__Visiblity of button********************************************************************************************

//*********************************************************************calling setting all buttons color  function after giving some time to load and repeating the process*************************************************

//

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                // Your logic here...

                // When you need to modify a UI element, do so on the UI thread.
                // 'getActivity()' is required as this is being ran from a Fragment.
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // This code will always run on the UI thread, therefore is safe to modify UI elements.


//****************************************if user is not clicking life gets reduced***********************************************************
                        lifeLevel=lifeLevel-2;
                        Life.setProgressValue(lifeLevel);

                        if (lifeLevel < 20) {
                            lifeLevel = 0;//..............................................................................................................setting to zero else going to minus
                            //Gameover
                            disable(SingleParent);//timer gets cancelled and the app crashes
//                            timer.cancel();//.........stopping the timer otherwise it is calling ShowPopupGameOver again and again and causing unneccessary vibrations

//                            Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
//                            // Vibrate for 500 milliseconds
//                            vib.vibrate(500);//.....................................................................................................................................vibrating Device

                            ShowPopupGameOver(playerPoints);

                        }
                        if (lifeLevel > 80) {
                            Life.setWaveColor(waveFull);//........................................................when life is full color
                        }


                        if (80 > lifeLevel && lifeLevel > 40) {
                            Life.setWaveColor(wavecolor);//........................................................when life is in between full color and die color
                        }


                        if (lifeLevel < 40) {
                            Life.setWaveColor(waveDie);//........................................................when life is about to die color
                        }

                        Life.setProgressValue(lifeLevel);

//****************************************if user is not clicking life gets reduced***********************************************************

                        allButtonRandomChange4();
                        allButtonRandomChange6();
                        allButtonRandomChange2();
                        allButtonRandomChange5();
                        allButtonRandomChange3();
                        allButtonRandomChange1();


                    }
                });
            }
        }, 0, buttonChangeColortime + 5000); // delay	: delay in milliseconds before task is to be executed.  period	:time in milliseconds between successive task executions.


//*********************************************************************END__calling setting all buttons color function after giving some time to load and repeating the process*************************************************


        //*************************************************Animation************************************************
// http://evgenii.com/blog/spring-button-animation-on-android/

        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        clickedButton.startAnimation(myAnim);
//*************************************************End___Animation************************************************

        // Log.e("insideclick:::::", playerColorSelected);


        String playerClickedColor = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());

        // playerColorSelectedInt = clickedButton.getBackgroundTintList().getDefaultColor();//.......................................................integer of color selected(actual color)

//        String hexColor = String.format("#%06X", (0xFFFFFF & playerColorSelectedInt));//.......................................................convert above hexdeimal to color code
//
//      playerColor=Color.parseColor(hexColor);
//
//        Log.e("COLOrCODE:::::", hexColor+" "+playerColorSelectedInt+" "+playerColor);


        if (playerColorSelected.contentEquals(playerClickedColor)) {
            playerPoints = playerPoints + 2;
            lifeLevel = lifeLevel + 20;

//            if (playerPoints > 10) {


                //Level UP
//                final TextView textView123 = (TextView) findViewById(R.id.textView123);
//
//                textView123.setText("SPEED UP");
//
//                final Handler GoVisiblity = new Handler();//...............................................pausing "GO" for one second and then making it invisible
//                GoVisiblity.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        textView123.setVisibility(View.GONE);
//                    }
//                }, 1000);
//                Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
//
//
//


//            }

            if (lifeLevel > 100) {
                lifeLevel = 100;//..............................................................................................................so that it should not go above 100
            }
            if (lifeLevel > 80) {
                Life.setWaveColor(waveFull);
            }

            if (80 > lifeLevel && lifeLevel > 40) {
                Life.setWaveColor(wavecolor);
            }

            if (lifeLevel < 40) {
                Life.setWaveColor(waveDie);
            }

            Life.setProgressValue(lifeLevel);


        } else {
            playerPoints--;
            lifeLevel = lifeLevel - 20;
//            Log.e("lifelevel::::::::::::", String.valueOf(lifeLevel));
            if (lifeLevel < 20) {
                lifeLevel = 0;//..............................................................................................................setting to zero else going to minus
                //Gameover
//                timer.cancel();
                disable(SingleParent);//timer gets cancelled and the app crashes
//                Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
//                // Vibrate for 500 milliseconds
//                vib.vibrate(500);//.....................................................................................................................................vibrating Device

                ShowPopupGameOver(playerPoints);

            }
            if (lifeLevel > 80) {
                Life.setWaveColor(waveFull);//........................................................when life is full color
            }


            if (80 > lifeLevel && lifeLevel > 40) {
                Life.setWaveColor(wavecolor);//........................................................when life is in between full color and die color
            }


            if (lifeLevel < 40) {
                Life.setWaveColor(waveDie);//........................................................when life is about to die color
            }

            Life.setProgressValue(lifeLevel);

        }


        singlePlayerPoints.setText(String.valueOf(playerPoints));

        //....................................................................................calling setting button color function after giving some time to load

        final Handler handlerclickedbutton = new Handler();
        handlerclickedbutton.postDelayed(new Runnable() {
            @Override
            public void run() {
                setNewColorToButton(clickedButton);
            }
        }, timeToReaapear);

        timeToReaapear = timeToReaapear - 20;


    }


    //*************************************************************************************************Popup for selecting Color**********************************************************
    public void ShowPopup(SinglePlayer v) {


//        TextView txtclose;
        final FloatingActionButton sbutton1, sbutton2, sbutton3, sbutton4, sbutton5,
                sbutton6, sbutton7, sbutton8, sbutton9, sbutton10,
                sbutton11;


        myDialog.setContentView(R.layout.customplayerselection);

        myDialog.setCanceledOnTouchOutside(false);//............................................................................................................diabling touching outside the myDialog box.
        myDialog.setCancelable(false);//.........................disabling the cancel button from closing the dialog

//        txtclose = (TextView) myDialog.findViewById(R.id.txtclose);

        sbutton1 = (FloatingActionButton) myDialog.findViewById(R.id.sbutton1);
        sbutton2 = (FloatingActionButton) myDialog.findViewById(R.id.sbutton2);
        sbutton3 = (FloatingActionButton) myDialog.findViewById(R.id.sbutton3);
        sbutton4 = (FloatingActionButton) myDialog.findViewById(R.id.sbutton4);
        sbutton5 = (FloatingActionButton) myDialog.findViewById(R.id.sbutton5);
        sbutton6 = (FloatingActionButton) myDialog.findViewById(R.id.sbutton6);
        sbutton7 = (FloatingActionButton) myDialog.findViewById(R.id.sbutton7);
        sbutton8 = (FloatingActionButton) myDialog.findViewById(R.id.sbutton8);
        sbutton9 = (FloatingActionButton) myDialog.findViewById(R.id.sbutton9);
        sbutton10 = (FloatingActionButton) myDialog.findViewById(R.id.sbutton10);
        sbutton11 = (FloatingActionButton) myDialog.findViewById(R.id.sbutton11);


//        txtclose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                myDialog.dismiss();
//            }
//        });


        View.OnClickListener PlayerColorClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final FloatingActionButton clickedButton;
                clickedButton = (FloatingActionButton) myDialog.findViewById(v.getId());

                //*************************************************Animation************************************************
//............................................................................................................................................ http://evgenii.com/blog/spring-button-animation-on-android/

                final Animation myAnim = AnimationUtils.loadAnimation(myDialog.getContext(), R.anim.bounce);

                // ..............................................................................................................................Use bounce interpolator with amplitude 0.2 and frequency 20
                MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                myAnim.setInterpolator(interpolator);

                clickedButton.startAnimation(myAnim);
//*************************************************End___Animation************************************************


                switch (v.getId()) {

                    case R.id.sbutton1:
                        playerColorSelected = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());
                        playerColorSelectedInt = clickedButton.getBackgroundTintList().getDefaultColor();
//                        Log.e("sbutton1color:::::::", playerColorSelected + "....." + playerColorSelectedInt);

                        break;

                    case R.id.sbutton2:
                        playerColorSelected = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());
                        playerColorSelectedInt = clickedButton.getBackgroundTintList().getDefaultColor();
//                        Log.e("sbutton2color:::::::", playerColorSelected + "....." + playerColorSelectedInt);

                        break;

                    case R.id.sbutton3:
                        playerColorSelected = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());
                        playerColorSelectedInt = clickedButton.getBackgroundTintList().getDefaultColor();
//                        Log.e("sbutton3color:::::::", playerColorSelected + "....." + playerColorSelectedInt);
                        break;
                    case R.id.sbutton4:
                        playerColorSelected = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());
                        playerColorSelectedInt = clickedButton.getBackgroundTintList().getDefaultColor();
//                        Log.e("sbutton4color:::::::", playerColorSelected + "....." + playerColorSelectedInt);

                        break;
                    case R.id.sbutton5:
                        playerColorSelected = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());
                        playerColorSelectedInt = clickedButton.getBackgroundTintList().getDefaultColor();
//                        Log.e("sbutton5color:::::::", playerColorSelected + "....." + playerColorSelectedInt);

                        break;
                    case R.id.sbutton6:
                        playerColorSelected = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());
                        playerColorSelectedInt = clickedButton.getBackgroundTintList().getDefaultColor();
//                        Log.e("sbutton6color:::::::", playerColorSelected + "....." + playerColorSelectedInt);

                        break;
                    case R.id.sbutton7:
                        playerColorSelected = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());
                        playerColorSelectedInt = clickedButton.getBackgroundTintList().getDefaultColor();
//                        Log.e("sbutton7color:::::::", playerColorSelected + "....." + playerColorSelectedInt);

                        break;
                    case R.id.sbutton8:
                        playerColorSelected = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());
                        playerColorSelectedInt = clickedButton.getBackgroundTintList().getDefaultColor();
//                        Log.e("sbutton8color:::::::", playerColorSelected + "....." + playerColorSelectedInt);
//                        myDialog.dismiss();
                        break;
                    case R.id.sbutton9:
                        playerColorSelected = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());
                        playerColorSelectedInt = clickedButton.getBackgroundTintList().getDefaultColor();
//                        Log.e("sbutton9color:::::::", playerColorSelected + "....." + playerColorSelectedInt);

                        break;
                    case R.id.sbutton10:
                        playerColorSelected = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());
                        playerColorSelectedInt = clickedButton.getBackgroundTintList().getDefaultColor();
                        Log.e("sbutton10color:::::::", playerColorSelected + "....." + playerColorSelectedInt);

                        break;
                    case R.id.sbutton11:
                        playerColorSelected = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());
                        playerColorSelectedInt = clickedButton.getBackgroundTintList().getDefaultColor();
//                        Log.e("sbutton11color:::::::", playerColorSelected + "....." + playerColorSelectedInt);

                        break;

                    default:
                        break;
                }

//************************************************************************setting points and score Color as Selected by player**************************************************
                hexColor = String.format("#%06X", (0xFFFFFF & playerColorSelectedInt));//.......................................................convert above hexdeimal to color code
                Log.e("HexColor", hexColor);
                singlePlayerPoints.setTextColor(Color.parseColor(hexColor));//....................................................settting the color of score and points as color selected by the players
                score.setTextColor(Color.parseColor(hexColor));//....................................................settting the color of score and points as color selected by the players
//************************************************************************End_setting points and score Color as Selected by player**************************************************

                final Handler dismissDialog = new Handler();
                dismissDialog.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        myDialog.dismiss();
                    }
                }, 300);//........................................................................................................to show the bounce animation in dialog box hence 300 milliseconds


//**************************************************************************************************************321 Start************************************************************************************************************
                final Handler handler321 = new Handler();
                final TextView textView123 = (TextView) findViewById(R.id.textView123);
                final java.util.concurrent.atomic.AtomicInteger n = new AtomicInteger(3);

                Animation RightSwipe = AnimationUtils.loadAnimation(myDialog.getContext(), R.anim.right_swipe);
                SingleParent.startAnimation(RightSwipe);//.............................................................................layout animation moving to right
                SingleParent.setVisibility(View.VISIBLE);//.....................................................................making relative layout visible since myDailog is closed


//                MediaPlayer buttonBounce = MediaPlayer.create(myDialog.getContext(), R.raw.gomusic);
//                buttonBounce.start();

                final Runnable counter = new Runnable() {

                    @Override
                    public void run() {
                        textView123.setText(Integer.toString(n.get()));
                        if (n.getAndDecrement() >= 1)
                            handler321.postDelayed(this, 380);
                        else {

                            textView123.setText("GO");

                            final Handler GoVisiblity = new Handler();//...............................................pausing "GO" for one second and then making it invisible
                            GoVisiblity.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    textView123.setVisibility(View.GONE);
                                }
                            }, 1000);
                            Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

                            // Vibrate for 500 milliseconds
                            vib.vibrate(500);//......................................................................vibrating Device
                            enable(SingleParent);//......................................................................enabling view after running 3 2 1
                            // button23.performClick();//...................................intial click to start the process
                        }
                    }
                };
                handler321.postDelayed(counter, 500);//.............time taken to start displaying 321
//************************************************************************************************************END__321 Start*********************************************************************************


            }
        };


        //..............................................................................................................................................................................................selection of color set to playerColorClick Listener
        sbutton1.setOnClickListener(PlayerColorClick);
        sbutton2.setOnClickListener(PlayerColorClick);
        sbutton3.setOnClickListener(PlayerColorClick);
        sbutton4.setOnClickListener(PlayerColorClick);
        sbutton5.setOnClickListener(PlayerColorClick);
        sbutton6.setOnClickListener(PlayerColorClick);
        sbutton7.setOnClickListener(PlayerColorClick);
        sbutton8.setOnClickListener(PlayerColorClick);
        sbutton9.setOnClickListener(PlayerColorClick);
        sbutton10.setOnClickListener(PlayerColorClick);
        sbutton11.setOnClickListener(PlayerColorClick);


        myDialog.getWindow().getAttributes().windowAnimations = R.style.animationName;
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();

    }
    //*************************************************************************************************END__Popup for selecting Color**********************************************************


    //*************************************************************************************************Popup after Game Over**********************************************************
    public void ShowPopupGameOver(int currentScore) {

        timer.cancel();//.........stopping the timer otherwise it is calling ShowPopupGameOver again and again and causing unneccessary vibrations

        Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        vib.vibrate(500);//.....................................................................................................................................vibrating Device


//int highScore;
        TextView currentScoretxtv, highScoretxtv;
        Button btnRestart, btnQuit;

        myDialog.setContentView(R.layout.customgameover);

        myDialog.setCanceledOnTouchOutside(false);//............................................................................................................diabling touching outside the myDialog box.
        myDialog.setCancelable(false);//.........................disabling the cancel button from closing the dialog

        currentScoretxtv = (TextView) myDialog.findViewById(R.id.currentScore);
        btnRestart = (Button) myDialog.findViewById(R.id.btnrestartss);
        btnQuit = (Button) myDialog.findViewById(R.id.btnquitss);
        highScoretxtv = (TextView) myDialog.findViewById(R.id.HighScore);

        currentScoretxtv.setText("Score: " + currentScore);

        SharedPreferences sharedHighScore = getSharedPreferences("HIGH_SCORE", Context.MODE_PRIVATE);//............using shared preference for highscores
        int highscore = sharedHighScore.getInt("HIGH_SCORE", 0);

        if (currentScore > highscore) {
            highScoretxtv.setText("High Score :" + currentScore);

            //update HighScore
            SharedPreferences.Editor editor = sharedHighScore.edit();
            editor.putInt("HIGH_SCORE", currentScore);
            editor.commit();
        } else {
            highScoretxtv.setText("High Score :" + highscore);
        }

//        if (currentScore > HighScore) {
//            highScoretxtv.setText("HIGHSCORE :"+ currentScore);
//            HighScore=currentScore;
//        }
//        else
//        {
//            highScoretxtv.setText("HIGHSCORE :"+ HighScore);
//        }

//        SharedPreferences.Editor editor = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE).edit();//............using shared preference for highscores
//
//        editor.putInt("checkHighScore", currentScore);
//        editor.apply();


        btnQuit.setOnClickListener(new View.OnClickListener() {//.............................................Quit Button
            @Override
            public void onClick(View view) {

//...................................................passing EXIT in intent and clearly activity stack so that this activity can recognise that it has been called before and should not reload again
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("EXIT", true);
                startActivity(i);


            }
        });

        btnRestart.setOnClickListener(new View.OnClickListener() {//.............................................Restart Button
            @Override
            public void onClick(View view) {


                Intent intent = getIntent();
                finish();
                startActivity(intent);

            }
        });

//        txtclose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                myDialog.dismiss();
//            }
//        });

        myDialog.getWindow().getAttributes().windowAnimations = R.style.animationName;//...................showing animation while popup appears
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();


    }
    //*************************************************************************************************END__Popup after Game Over**********************************************************


    //*************************************************************************************************************************Enabling and Disabling View************************************************************************************
    private static void disable(ViewGroup layout) {
        layout.setEnabled(false);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                disable((ViewGroup) child);
            } else {
                child.setEnabled(false);
            }
        }
    }

    private static void enable(ViewGroup layout) {
        layout.setEnabled(true);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                enable((ViewGroup) child);
            } else {
                child.setEnabled(true);
            }
        }
    }
//*************************************************************************************************************************END__Enabling and Disabling View************************************************************************************


    //*******************************************************************Randomising and Setting new color to the the button function*************************************************
    private void setNewColorToButton(FloatingActionButton clickedButton) {


        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(12 - 1) + 1;    //..........numver betwween 1(inclusive) and 5(exclusive).....

        //..........if the number is zero.....
        //myRandomColor++;


        switch (myRandomColor) {
            case 1:

                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s1));

                break;
            case 2:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s2));
                break;
            case 3:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s3));
                break;
            case 4:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s4));
                break;
            case 5:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s5));
                break;
            case 6:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s6));
                break;
            case 7:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s7));
                break;
            case 8:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s8));
                break;
            case 9:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s9));
                break;
            case 10:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s10));
                break;
            case 11:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s11));
                break;
            default:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), playerColorSelectedInt));//......................................................no use


        }


    }

//*******************************************************************END__Randomising and Setting new color to the the button function*************************************************


    //******************************************************************Randomly changing colors of buttons******************************************************************
    private void allButtonRandomChange1() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................numver betwween 1(inclusive) and 5(exclusive)

        switch (myRandomColor) {
            case 1:
                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s1));
//                button2.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button3.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button1.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));

                button25.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 2:
                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s2));
                button10.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s3));
//                button3.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button2.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button26.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 3:
                button3.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s4));
                button9.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s5));
                button15.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s6));
//                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button3.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button27.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 4:
                button2.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s7));
                button8.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s8));
                button14.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s9));
                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s10));
//                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button4.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;

            default:
                button1.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));//...............................................................................................no use
//            button7.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//            button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//            button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//            button25.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange2() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //..........numver betwween 1(inclusive) and 5(exclusive).....

        switch (myRandomColor) {
            case 1:
                button1.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s11));
                button7.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s10));
                button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s9));
                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s8));
                button25.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s7));

                button5.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button28.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 2:
                button6.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s6));
                button12.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s5));
                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s4));
                button24.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s3));
                button30.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s2));

                button6.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button29.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 3:
                button11.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s1));
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s2));
                button23.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s3));
                button29.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s4));
                button35.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s5));

                button7.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button30.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 4:
                button16.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s6));
                button22.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s7));
                button28.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s8));
                button34.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s9));
                button40.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s10));
//                button35.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));

                button8.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button31.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;

            default:
                button16.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));//.........................................................no use
//                button22.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button28.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button34.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button35.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange3() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................number betwween 1(inclusive) and 5(exclusive)

        switch (myRandomColor) {
            case 1:
                button21.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s11));
                button27.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s10));
                button33.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s9));
                button39.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s8));
                button45.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s7));
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));

                button9.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button32.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));

                break;
            case 2:
                button26.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s6));
                button32.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s5));
                button38.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s4));
                button44.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s3));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button10.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button33.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 3:
                button31.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s2));
                button37.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s1));
                button43.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s2));
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button11.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button34.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 4:
                button1.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s3));
                button36.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s4));
                button42.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s5));
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button12.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button35.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;

            default:
                button1.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));//.........................................................no use
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange4() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................number between 1(inclusive) and 5(exclusive).....

        switch (myRandomColor) {
            case 1:
                button6.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s6));
                button2.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s7));
                button41.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s8));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button13.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button36.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 2:
                button11.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s9));
                button7.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s10));
                button3.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s11));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button14.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button37.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 3:
                button16.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s10));
                button12.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s9));
                button8.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s8));
                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s7));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button15.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button38.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 4:
                button21.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s6));
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s5));
                button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s4));
                button9.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s3));
                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s2));

                button16.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button39.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;

            default:
                button21.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));//.......................................................................no use
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button9.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange5() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................numver betwween 1(inclusive) and 5(exclusive).....

        switch (myRandomColor) {
            case 1:
                button26.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s1));
                button22.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s2));
                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s3));
                button14.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s4));
                button10.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s5));

                button17.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button40.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 2:
                button31.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s6));
                button27.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s7));
                button23.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s8));
                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s9));
                button15.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s10));

                button18.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button41.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 3:
                button36.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s11));
                button32.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s10));
                button28.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s9));
                button24.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s8));
                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s7));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));

                button19.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button42.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 4:
                button41.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s6));
                button37.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s5));
                button33.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s4));
                button29.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s3));
                button25.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s2));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));

                button20.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button43.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;

            default:
                button33.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));//.......................................................................no use
                button29.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button25.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange6() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................numver betwween 1(inclusive) and 5(exclusive).....

        switch (myRandomColor) {
            case 1:
                button42.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s1));
                button38.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s2));
                button34.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s3));
                button30.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s4));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));

                button21.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button44.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 2:
                button35.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s5));
                button39.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s6));
                button43.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s7));
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button22.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button45.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 3:
                button44.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s8));
                button40.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s9));
                button21.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s10));
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s11));
                button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s10));
                button9.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s9));
                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s8));

                button23.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button1.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                break;
            case 4:
                button45.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s7));
                button26.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s6));
                button22.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s5));
                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s4));
                button14.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s3));
                button10.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.s2));

                button24.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));
                button23.setBackgroundTintList(ColorStateList.valueOf(playerColorSelectedInt));//.....................................color to middle button just ...making no sense
                break;

            default:
                button16.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));//.....................................................................................no use
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }
//******************************************************************END__Randomly changing colors of buttons******************************************************************

//    @Override
//    public void onBackPressed() {
//
//        Intent i = new Intent(getApplicationContext(), MainActivity.class);
//        startActivity(i);
//    }


}


