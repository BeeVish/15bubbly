package com.beevishapps.hitright;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.igalata.bubblepicker.BubblePickerListener;
import com.igalata.bubblepicker.adapter.BubblePickerAdapter;
import com.igalata.bubblepicker.model.BubbleGradient;
import com.igalata.bubblepicker.model.PickerItem;
import com.igalata.bubblepicker.rendering.BubblePicker;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;


public class MainActivity extends AppCompatActivity {


    BubblePicker picker;

    //public static int centerImmediate=0;
    Button play;
    ImageView titleimage;
    TextView shakeTitle;

    //Quit popup code
    Dialog myDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        shakeTitle = (TextView) findViewById(R.id.subtitleTextView);
        shakeTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(), SinglePlayer.class);//........................opening single player while clicking the shaking text
                startActivity(i);

            }
        });
//        Intent svc=new Intent(getApplicationContext(), BackgroundSoundService.class);
//        startService(svc);//................................................................................................................starting bubbling sound


//        AsyncTask.execute(new Runnable() {
//            @Override
//            public void run() {
//                MediaPlayer bubblesmp3 = MediaPlayer.create(getApplicationContext(), R.raw.bubblesmp3);
//                bubblesmp3.start();
//                bubblesmp3.setLooping(true);
//            }
//        });

        myDialog = new Dialog(this);//.......................................................................................................for pop up layout

// ***********************************************************Displaying titleImage using picasso since large size images are not able to display by conventional method***********
        ImageView titleimage = (ImageView) findViewById(R.id.titleImageView);
        TextView subtitletextview = (TextView) findViewById(R.id.subtitleTextView);
        TextView hintTextView = (TextView) findViewById(R.id.hintTextView);
        Picasso.with(this).load(R.drawable.btitleimage).resize(2900, 1000)
                .into(titleimage);


        final Animation fortxt = AnimationUtils.loadAnimation(getBaseContext(), R.anim.shake);
        final Animation fortxt2 = AnimationUtils.loadAnimation(getBaseContext(), R.anim.animation_on);
        final Animation forimage = AnimationUtils.loadAnimation(getBaseContext(), R.anim.slide_down);
//        final Animation an2= AnimationUtils.loadAnimation(getBaseContext(),R.anim.right_swipe);

        subtitletextview.startAnimation(fortxt);
        titleimage.startAnimation(forimage);
        hintTextView.startAnimation(fortxt2);
//        an1.setRepeatCount(Animation.INFINITE);

//***********************************************************END__Displaying titleImage using picasso since large size images are not able to display by conventional method***********

        picker = (BubblePicker) findViewById(R.id.picker);


//        if(centerImmediate>=1) {
//            picker.setCenterImmediately(true);//............................................................................................................put bubbles in center view immediately on launch
//        }
//        centerImmediate++;
        picker.setBubbleSize(35);//.................................................................................................................................................set size of bubbles

        final String[] titles = getResources().getStringArray(R.array.countries);
        final TypedArray colors = getResources().obtainTypedArray(R.array.colors);
        //final TypedArray images = getResources().obtainTypedArray(R.array.images);


        picker.setAdapter(new BubblePickerAdapter() {


            @Override
            public int getTotalCount() {
                return titles.length;
            }

            @NotNull
            @Override
            public PickerItem getItem(int position) {
                PickerItem item = new PickerItem();
                item.setTitle(titles[position]);
                item.setGradient(new BubbleGradient(colors.getColor((position * 2) % 8, 0),
                        colors.getColor((position * 2) % 8 + 1, 0), BubbleGradient.VERTICAL));


                //font inside bubbles
                Typeface mediumTypeface = Typeface.createFromAsset(getAssets(), "roboto_medium.ttf");

                item.setTypeface(mediumTypeface);//...................................................................................................................................setting font inside bubbles
                item.setTextColor(ContextCompat.getColor(MainActivity.this, android.R.color.white));
                // item.setBackgroundImage(ContextCompat.getDrawable(MainActivity.this, images.getResourceId(position, 0)));
                return item;
            }
        });

        picker.setListener(new BubblePickerListener() {


            @Override
            public void onBubbleSelected(@NotNull PickerItem item) {

                //Log.e("out::::::::::", item.getTitle());
                if ((item.getTitle()).contentEquals("MultiPlayer")) {

                    Intent i = new Intent(getApplicationContext(), GameActivity.class);
                    startActivity(i);


                }


                if ((item.getTitle()).contentEquals("Quit")) {

                    ShowPopup();
                }

                if ((item.getTitle()).contentEquals("SinglePlayer")) {

                    Intent i = new Intent(getApplicationContext(), SinglePlayer.class);
                    startActivity(i);
                }

                if ((item.getTitle()).contentEquals("MultiSection")) {


                    Intent i = new Intent(getApplicationContext(), MultiSection.class);
                    startActivity(i);

                }

                if ((item.getTitle()).contentEquals("Credits")) {

                    Intent i = new Intent(getApplicationContext(), Credits.class);
                    startActivity(i);


                }
                if ((item.getTitle()).contentEquals("HwToPlay")) {

                    Intent i = new Intent(getApplicationContext(), HwToPlay.class);
                    startActivity(i);


                }

                if ((item.getTitle()).contentEquals("HighScores")) {

                    Intent i = new Intent(getApplicationContext(), HighscoresActivity.class);
                    startActivity(i);


                }

                if ((item.getTitle()).contentEquals("RateUs")) {

                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.beevishapps.hitright&hl=en" + "Bubbly")));


                }

                if ((item.getTitle()).contentEquals("Share")) {

                    try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, "Bubbly");
                        String sAux = "\nDo Check This Game Out\n\n";
                        sAux = sAux + "https://play.google.com/store/apps/details?id=com.beevishapps.hitright&hl=en \n\n";
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "Thank u for Sharing!!Choose Any one"));
                    } catch(Exception e) {
                        //e.toString();
                    }

                }


            }

            @Override
            public void onBubbleDeselected(@NotNull PickerItem item) {

            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        picker.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        picker.onPause();
//        Intent svc=new Intent(getApplicationContext(), BackgroundSoundService.class);
//        stopService(svc);//................................................................................................................stopping bubbling sound
    }

    @Override
    protected void onStop() {
        super.onStop();
//        Intent svc=new Intent(getApplicationContext(), BackgroundSoundService.class);
//        stopService(svc);//................................................................................................................stopping bubbling sound

    }


    //*************************************************************************************************Popup for quitting the Game**********************************************************
    public void ShowPopup() {


        TextView txtclose;
        Button btnYes, btnNo;

        myDialog.setContentView(R.layout.customquitpopup);

        myDialog.setCanceledOnTouchOutside(false);//............................................................................................................diabling touching outside the myDialog box.


//        txtclose = (TextView) myDialog.findViewById(R.id.txtclose);
        btnYes = (Button) myDialog.findViewById(R.id.btnyes);
        btnNo = (Button) myDialog.findViewById(R.id.btnno);


        btnNo.setOnClickListener(new View.OnClickListener() {//.............................................Quit Button
            @Override
            public void onClick(View view) {

                myDialog.dismiss();
            }
        });

        btnYes.setOnClickListener(new View.OnClickListener() {//.............................................Restart Button
            @Override
            public void onClick(View view) {


                finish();

            }
        });

//        txtclose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                myDialog.dismiss();
//            }
//        });

        myDialog.getWindow().getAttributes().windowAnimations = R.style.animationName;//...........................showing animation while popup comes
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();

    }
    //*************************************************************************************************END__Popup for quitting the game**********************************************************


//    @Override
//    public void onBackPressed() {
//
//        ShowPopup();
//    }
}
