package com.beevishapps.hitright;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {


    FloatingActionButton button1, button2, button3, button4, button5,
            button6, button7, button8, button9, button10,
            button11, button12, button13, button14, button15,
            button16, button17, button18, button19, button20,
            button21, button22, button23, button24, button25,
            button26, button27, button28, button29, button30,
            button31, button32, button33, button34, button35;

    RelativeLayout MultiParent;

    TextView p1Points, p2Points;
    int timeToReaapear = 3000;
    int visibilityToReaapear = 7000;
    int buttonChangeColortime = 2000;
    int player1points = 0, player2points = 0;


    //popup code
    Dialog myDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


//
//Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (getIntent().getBooleanExtra("EXIT", false))//..........................................................so that after quit button is clicked this page should not get reloaded....if the user wants to finally exit
        {
            finish();
//            Intent intent = new Intent(Intent.ACTION_MAIN);
//            intent.addCategory(Intent.CATEGORY_HOME);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
        }


        setContentView(R.layout.activity_game);

//        Intent svc=new Intent(getApplicationContext(), BackgroundSoundService.class);
//        stopService(svc);//................................................................................................................stopping bubbling sound



        Animation LeftSwipe = AnimationUtils.loadAnimation(this, R.anim.left_swipe);//..........................................loading animation to layout



        MultiParent = (RelativeLayout) findViewById(R.id.MultiParent);
        MultiParent.startAnimation(LeftSwipe);//.............................................................................layout animation moving to Left

        disable(MultiParent);//................................................................................................................disabling view for running 3 2 1
        //**************************************************************************************************************321 Start************************************************************************************************************
        final Handler handler321 = new Handler();
        final TextView textView123 = (TextView) findViewById(R.id.textView123);

        final java.util.concurrent.atomic.AtomicInteger n = new AtomicInteger(3);

//        MediaPlayer buttonBounce = MediaPlayer.create(this, R.raw.gomusic);
//        buttonBounce.start();

        final Runnable counter = new Runnable() {
            @Override
            public void run() {
                textView123.setText(Integer.toString(n.get()));
                if (n.getAndDecrement() >= 1)
                    handler321.postDelayed(this, 380);
                else {
                    textView123.setText("GO");

                    final Handler GoVisiblity = new Handler();//....................................................................pausing "GO" for one second and then making it invisible
                    GoVisiblity.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            textView123.setVisibility(View.GONE);
                        }
                    }, 1000);


                    // start the game
                    Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    vib.vibrate(500);//................................................................................................................vibrating Device

                    enable(MultiParent);//................................................................................................................enabling view after running 3 2 1
                    button18.performClick();//................................................................................intial click to start the process
                    button19.performClick();
                }
            }
        };
        handler321.postDelayed(counter, 500);//............................................................................time taken to start displaying 321
//************************************************************************************************************END__321 Start*********************************************************************************


        myDialog = new Dialog(this);//.......................................................................................................for pop up layout

        //Reference all the elements of our UI with an appropriate object


        //First the TextViews
        p1Points = (TextView) findViewById(R.id.p1Points);
        p2Points = (TextView) findViewById(R.id.p2Points);

        //Now the buttons
        button1 = (FloatingActionButton) findViewById(R.id.button1);
        button2 = (FloatingActionButton) findViewById(R.id.button2);
        button3 = (FloatingActionButton) findViewById(R.id.button3);
        button4 = (FloatingActionButton) findViewById(R.id.button4);
        button5 = (FloatingActionButton) findViewById(R.id.button5);
        button6 = (FloatingActionButton) findViewById(R.id.button6);
        button7 = (FloatingActionButton) findViewById(R.id.button7);
        button8 = (FloatingActionButton) findViewById(R.id.button8);
        button9 = (FloatingActionButton) findViewById(R.id.button9);
        button10 = (FloatingActionButton) findViewById(R.id.button10);
        button11 = (FloatingActionButton) findViewById(R.id.button11);
        button12 = (FloatingActionButton) findViewById(R.id.button12);
        button13 = (FloatingActionButton) findViewById(R.id.button13);
        button14 = (FloatingActionButton) findViewById(R.id.button14);
        button15 = (FloatingActionButton) findViewById(R.id.button15);
        button16 = (FloatingActionButton) findViewById(R.id.button16);
        button17 = (FloatingActionButton) findViewById(R.id.button17);
        button18 = (FloatingActionButton) findViewById(R.id.button18);
        button19 = (FloatingActionButton) findViewById(R.id.button19);
        button20 = (FloatingActionButton) findViewById(R.id.button20);
        button21 = (FloatingActionButton) findViewById(R.id.button21);
        button22 = (FloatingActionButton) findViewById(R.id.button22);
        button23 = (FloatingActionButton) findViewById(R.id.button23);
        button24 = (FloatingActionButton) findViewById(R.id.button24);
        button25 = (FloatingActionButton) findViewById(R.id.button25);
        button26 = (FloatingActionButton) findViewById(R.id.button26);
        button27 = (FloatingActionButton) findViewById(R.id.button27);
        button28 = (FloatingActionButton) findViewById(R.id.button28);
        button29 = (FloatingActionButton) findViewById(R.id.button29);
        button30 = (FloatingActionButton) findViewById(R.id.button30);
        button31 = (FloatingActionButton) findViewById(R.id.button31);
        button32 = (FloatingActionButton) findViewById(R.id.button32);
        button33 = (FloatingActionButton) findViewById(R.id.button33);
        button34 = (FloatingActionButton) findViewById(R.id.button34);
        button35 = (FloatingActionButton) findViewById(R.id.button35);


        //Now set all the buttons to listen for clicks
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);
        button10.setOnClickListener(this);
        button11.setOnClickListener(this);
        button12.setOnClickListener(this);
        button13.setOnClickListener(this);
        button14.setOnClickListener(this);
        button15.setOnClickListener(this);
        button16.setOnClickListener(this);
        button17.setOnClickListener(this);
        button18.setOnClickListener(this);
        button19.setOnClickListener(this);
        button20.setOnClickListener(this);
        button21.setOnClickListener(this);
        button22.setOnClickListener(this);
        button23.setOnClickListener(this);
        button24.setOnClickListener(this);
        button25.setOnClickListener(this);
        button26.setOnClickListener(this);
        button27.setOnClickListener(this);
        button28.setOnClickListener(this);
        button29.setOnClickListener(this);
        button30.setOnClickListener(this);
        button31.setOnClickListener(this);
        button32.setOnClickListener(this);
        button33.setOnClickListener(this);
        button34.setOnClickListener(this);
        button35.setOnClickListener(this);


    }

    //*************************************************************************************************************************Enabling and Disabling View************************************************************************************
    private static void disable(ViewGroup layout) {
        layout.setEnabled(false);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                disable((ViewGroup) child);
            } else {
                child.setEnabled(false);
            }
        }
    }

    private static void enable(ViewGroup layout) {
        layout.setEnabled(true);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            if (child instanceof ViewGroup) {
                enable((ViewGroup) child);
            } else {
                child.setEnabled(true);
            }
        }
    }
//*************************************************************************************************************************END__Enabling and Disabling View************************************************************************************

    @Override
    public void onClick(View v) {


        //getting button by its id according to its click

        final FloatingActionButton clickedButton;
        clickedButton = (FloatingActionButton) findViewById(v.getId());



//*******************************************************************Visiblity of button********************************************************************************************


        clickedButton.setVisibility(View.INVISIBLE);
        final Handler handlerclickedbuttonVisibility = new Handler();
        handlerclickedbuttonVisibility.postDelayed(new Runnable() {
            @Override
            public void run() {
                clickedButton.setVisibility(View.VISIBLE);
            }
        }, visibilityToReaapear);

        visibilityToReaapear = visibilityToReaapear - 20;


//*******************************************************************END__Visiblity of button********************************************************************************************

//media Sound for bounce
        //  MediaPlayer buttonBounce = MediaPlayer.create(this, R.raw.bb);

//*********************************************************************calling setting all buttons color  function after giving some time to load and repeating the process*************************************************


        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                // Your logic here...

                // When you need to modify a UI element, do so on the UI thread.
                // 'getActivity()' is required as this is being ran from a Fragment.
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // This code will always run on the UI thread, therefore is safe to modify UI elements.



                        allButtonRandomChange4();
                        allButtonRandomChange6();
                        allButtonRandomChange2();
                        allButtonRandomChange5();
                        allButtonRandomChange3();
                        allButtonRandomChange1();



                    }
                });
            }
        }, 0, buttonChangeColortime+5000); // the code will run twice(2) after specified time(buttonChangeColortime) for infinte period of time.


//*********************************************************************END__calling setting all buttons color function after giving some time to load and repeating the process*************************************************

//*************************************************Animation************************************************
// http://evgenii.com/blog/spring-button-animation-on-android/

        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);

        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        clickedButton.startAnimation(myAnim);
//*************************************************End___Animation************************************************


        // buttonBounce.start();//.....................................................................................................start media Sound for bounce


//***************set ripple programatically********************
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            clickedButton.setRippleColor(getColor(R.color.ripple));
//        }

        //Log.e(" color********",String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor()));

        String playerColor = String.valueOf(clickedButton.getBackgroundTintList().getDefaultColor());
        String white = "-1";
        String black = "-16777216";

//******************************************increment point to players and setting it*********************************************
        if (playerColor.equals(white)) {
            player1points=player1points+2;
        }
        if (playerColor.equals(black)) {
            player2points=player2points+2;
        }
//        Log.e("player1 points:", String.valueOf(player1points));
//        Log.e("player2 points:", String.valueOf(player2points));


        p1Points.setText(String.valueOf(player1points));
        p2Points.setText(String.valueOf(player2points));

        if (player1points == 100) {


            Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            vib.vibrate(500);//.....................................................................................................................................vibrating Device
            ShowPopup("PLAYER 1", this);
            player1points++;


        }
        if (player2points == 100) {

            Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            vib.vibrate(500);//.....................................................................................................................................vibrating Device
            ShowPopup("PLAYER 2", this);
            player2points++;

        }

//******************************************END__increment point to players and setting it*********************************************

        //................................................................................................................setting the backgroundTintList of button to white or we can say neutralising it.
        //clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.neutral));


//calling setting button color function after giving some time to load

        final Handler handlerclickedbutton = new Handler();
        handlerclickedbutton.postDelayed(new Runnable() {
            @Override
            public void run() {
                setNewColorToButton(clickedButton);
            }
        }, timeToReaapear);

        timeToReaapear = timeToReaapear - 20;

    }
    //******************************************************************Randomly changing colors of buttons******************************************************************
    private void allButtonRandomChange1() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................numver betwween 1(inclusive) and 5(exclusive)

        switch (myRandomColor) {
            case 1:
                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button2.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button3.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


                break;
            case 2:
                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button10.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button3.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 3:
                button3.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button9.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button15.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 4:
                button2.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button8.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button14.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;

            default:
                button1.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button7.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button25.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange2() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //..........numver betwween 1(inclusive) and 5(exclusive).....

        switch (myRandomColor) {
            case 1:
                button1.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button7.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button25.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


                break;
            case 2:
                button6.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button12.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button24.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button30.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;
            case 3:
                button11.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button23.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button29.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button35.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 4:
                button16.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button22.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button28.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button34.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button35.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;

            default:
                button16.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button22.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button28.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button34.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button35.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange3() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................number betwween 1(inclusive) and 5(exclusive)

        switch (myRandomColor) {
            case 1:
                button21.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button27.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button33.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


                break;
            case 2:
                button26.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button32.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 3:
                button31.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 4:
                button1.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;

            default:
                button1.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange4() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................number between 1(inclusive) and 5(exclusive).....

        switch (myRandomColor) {
            case 1:
                button6.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button2.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


                break;
            case 2:
                button11.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button7.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button3.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 3:
                button16.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button12.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button8.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button4.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 4:
                button21.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button9.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;

            default:
                button21.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button9.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange5() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................numver betwween 1(inclusive) and 5(exclusive).....

        switch (myRandomColor) {
            case 1:
                button26.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button22.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button14.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button10.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));


                break;
            case 2:
                button31.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button27.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button23.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button15.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;
            case 3:
                button32.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button28.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button24.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 4:
                button33.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button29.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button25.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;

            default:
                button33.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button29.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button25.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

    private void allButtonRandomChange6() {

        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //.........................................................................numver betwween 1(inclusive) and 5(exclusive).....

        switch (myRandomColor) {
            case 1:
                button34.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button30.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


                break;
            case 2:
                button35.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
//                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
//                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 3:
                button21.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button13.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button9.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button5.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 4:
                button26.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button22.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button14.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button10.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;

            default:
                button16.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button17.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button18.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                button19.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                button20.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }
//******************************************************************END__Randomly changing colors of buttons******************************************************************

    //*******************************************************************Randomising and Setting new color to the the Single button function*************************************************
    private void setNewColorToButton(FloatingActionButton clickedButton) {


        Random myRandomNumber = new Random();
        int myRandomColor;
        myRandomColor = myRandomNumber.nextInt(5 - 1) + 1;    //....................................................number betwween 1(inclusive) and 5(exclusive).....

        //..........if the number is zero.....
        //myRandomColor++;


        switch (myRandomColor) {
            case 1:

                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));

                break;
            case 2:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;
            case 3:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));
                break;
            case 4:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player2));
                break;

            default:
                clickedButton.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.player1));


        }


    }

//*******************************************************************END__Randomising and Setting new color to the the Single button function*************************************************


    //*************************************************************************************************Popup after winning the Game**********************************************************
    public void ShowPopup(String winner, GameActivity v) {


        TextView txtclose, txtWinner;
        Button btnRestart, btnQuit;

        myDialog.setContentView(R.layout.custompopup);


        myDialog.setCanceledOnTouchOutside(false);//............................................................................................................diabling touching outside the myDialog box.

        myDialog.setCancelable(false);//.........................disabling the cancel button from closing the dialog


//        txtclose = (TextView) myDialog.findViewById(R.id.txtclose);
        txtWinner = (TextView) myDialog.findViewById(R.id.winner);
        btnRestart = (Button) myDialog.findViewById(R.id.btnrestart);
        btnQuit = (Button) myDialog.findViewById(R.id.btnquit);

        txtWinner.setText(winner + " WINS");//............................................................................................................setting winner

        btnQuit.setOnClickListener(new View.OnClickListener() {//.............................................Quit Button
            @Override
            public void onClick(View view) {

//...................................................passing EXIT in intent and clearly activity stack so that this activity can recognise that it has been called before and should not reload again
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("EXIT", true);
                startActivity(i);


            }
        });

        btnRestart.setOnClickListener(new View.OnClickListener() {//.............................................Restart Button
            @Override
            public void onClick(View view) {


                Intent intent = getIntent();
                finish();
                startActivity(intent);

            }
        });

//        txtclose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                myDialog.dismiss();
//            }
//        });

        myDialog.getWindow().getAttributes().windowAnimations = R.style.animationName;
        myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        myDialog.show();

    }
    //*************************************************************************************************END__Popup after winning the game**********************************************************



//        @Override
//    public void onBackPressed() {
//
//            Intent i = new Intent(getApplicationContext(), MainActivity.class);
//            startActivity(i);
//    }

}
